
minetest.register_chatcommand("act", {
	params = "<act>",
	description = "preps stage for action",
	func = function(name, param)
		if param == "" then
			return false, "Invalid usage, see /help act."
		end

		minetest.log("action", "/act invoked, owner="..name)

		return true, "well acted!"
	end
})


