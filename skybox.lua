
macbest.sky_defaults = {
   "#cccccc",
   0.8,
   { 
      density = 0.25, 
      color = "#fffffffb", 
      ambient = "#000000",
      height = 120, 
      thickness = 8, 
      speed = {x = -2, y = 0}
   }
}

macbest.skies = {
      ["withBird"] = {},
      ["khM4e"] = {},
      ["earthatnight2012light"] = { "#882200", 0.8, { 
              density = 0.1, 
              color = "#ff6666", 
              ambient = "#770011", 
              thickness = 0.2
      }},
      ["dustTempLight"] = { "#cc8833", 0.8, { 
              density = 0.1, 
              color = "#ff6666", 
              ambient = "#eeff88", 
              height = 24, 
              speed = {x = 20, y = -2},
      }}
}


for sky_name, custom_def in pairs(macbest.skies) do
   sky_def = { [1] = sky_name }
   for i, default_def in ipairs(macbest.sky_defaults) do
     sky_def[i + 1] = custom_def[i] or default_def
   end
   minetest.log("action", "registering skybox "..sky_def[1])
   skybox.add(sky_def)
end


