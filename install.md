
# Some Assembly Required 

## About Macbest

MacBest is a Minetest mod.  It contains code (in LUA and Python) 
and example data which can be used to, amoung other things,  put 
The Scottish Play into a virtual Voxel world.

In order to make the Scottish play happen with voxels, we will 
need to download some pieces, edit some files, run some programs 
to try out different ways of doing things, tweak some data files, 
and experiment a bit in order to make the bare-bone Minetest 
framework do what we imagine the play to look like in a voxel 
world.

We need the right tools.

* tools to fetch stuff
* tools (programs) to translate, tweak and combine data 
* tools to tweak data 
* tools to add some custom code to Minetest

Below is a list of things we are likely to need for this project.

## Some Suggestions...

All software listed is free and open source, you do not need to
sign up for anything, pay anything, and there are no restrictions
on what you can do with it.

I have tried to make sure that all these can be installed on 
Mac OSX, Windows, and Linux systems.  In writing this, I am trying
to leave you a sufficient number of bread crumbs to make it to the 
other side without burdening you in details you may or may nor need
to know.

Please read one section at a time before making changes to your 
system.  This way, you will have more context on what you are 
doing which should help if you get lost.

If you get stuck, text/email/call me and I will do what I can to help.


# Installing MacBest Prerequisites

## Minetest 

In order to walk around in the virtual voxel worlds (your own 
or somebody else's), you will need Minetest installed on your 
end.

Instructions on how to do that can be found at https://www.minetest.net/

Your Minetest files will be installed in different places depending on 
what type of system you are on and how the installation is done.  However,
you will need to know where your Minetest mods/ folder is (or should be)
in order to install some mods later on.

The Minetest mods folder is created automaticall the first time you 
install a mod via the Minetest menu (from the Content Tab), which means
that you might have to create it by hand if it does not exist yet.

On Linux Mint 19, for instance, this is done with the following command
for one of the flavors of Minetest available for that OS:

```
    mkdir .var/app/net.minetest.Minetest/.minetest/mods
```

My point is that you will need to know where a Minetest user (who may
or may not be the only user on your system) needs to add their mods.

This is, largely, because, on Mac OS/X, Linux, and other operating 
systems, you migth have several people sharing the same disk, and Mintest 
has provisions for everybody to add their own magic without breaking 
things for everybody else.

See https://dev.minetest.net/Installing_Mods for general information
on how to install Minetest Mods.  


## Git

Git is a system for tracking and managing softwarre changes. If 
you are looking for open source software, chances are you can 
download a folder containing the whole thing from a web site 
using git.

Some software development environments have git built-in (see 
IntelliJ below).

Atlassian has a good web page on installing git:  

* https://www.atlassian.com/git/tutorials/install-git

You will need it for installing the macbest Minetest mod.


## Python

You will need to basic Python3 installed plus some additional 
libraries in order to use the python tools or to create sky boxes
or to generate new world maps using the geo-mapgen mod.

If you already have a recent Python3 installed, you should be ok.

Otherwise, you can follow these instructions:

* https://www.python.org/downloads/

Creating a virtual Python environment is a good way
to keep your changes from interfering with your other Python
projects.


## LUA

LUA is a simple and clean programming language designed for
new coders and often used to write and extend computer games.

* https://www.lua.org/start.html

Minetest comes with its own LUA built-in but having a proper
LUA installed will allow us to work on LUA code in a snazzy 
coding environment like IntelliJ IDEA. 

Remember where you parked your LUA SDK as you will need to
know this later.


## IntelliJ Development Environment (IDE)

IntelliJ IDEA is a professional grade software development environment 
which works across many operating systems.

It started out as a Java coding environment, but it can also speak 
LUA and Python, both of which we need.  It is a bit of a Swiss Army Knife 
Batmobil sort of thing.   At first, the many buttons and options 
can be a little daunting.  Fortunately, it's design is vert consistent, 
and there is lots of help online in the form of videos and Q&A sites.

First you download and install IntelliJ itself.  
The community edition is free and perfectly adequate for our purpose.

* https://www.jetbrains.com/idea/download/

Feel free to power it up and look around.  When you are done 
with that, bring up the Preferences->Plugins Screen (or click 
the gear icon at the bottom) and install ...

* Python Community Edition
* LUA

These plug-ins will allow it to understand LUA and Python, 
but you will need to tell it where to find the right LUA and 
Python on your machine (because you could have many different ones).

There are many more interesting plug-ins, many of which are
useful and others are essential for a different reason, like
the Nyan cat progress bar.

After you install a new plug-in (extension), you have to restart 
IDEA before those can be used.  


## Creating a Minetest Python Environment

You will need to download and install a number of scientific Python
code libraries.  For working with sky boxes, you will need ...

* numpy
* pillow (aka PIL)

and for working with geographical data files and digital images:

* gdal (aka osgeo)
* rasterio
* py360convert
* simpledem

It is ok to ignore these for now, but the first time you run Python 
code which needs them, you will get an error to remind you.

Some of these might require additional installation of scientific,
math or image processing libraries which may or may not be installed 
on your system yet.  Generally, if your IDE cannot install them for 
you (see below), you might have to google something like "python gdal 
windows install" and look for some recent instructions on how to make
things work.

Since not all versions of all of these libraries get
along with each other, it good practice to give each project it's own 
place to maintains it's Python code collection.  This is called a
"Python virtual environment", or _venv_ for short.

This way, you will not inadvertantly break other Python code installed
on your system.

From the IDE's start-up screen, go into Preferences (gear icon) / 
New Project Structure / Platform Settings / SDKs and click on '+' to 
create a Python3 venv under your Mintest folder, next to the mods 
folder.  

If your Minetest mods folder is .../someplace/.minetest/mods, then
the venv folder should be .../someplace/.minetest/venv.  We will use 
this 

We are not ready to download some Python and LUA code!


## Loading MacBest into IntellijIDE

On the IDEs startup screen you should see choices like "Create 
New Project", but what we want do is either "Open or Import" an 
existing folder or to "Get a Project From Version Control".

If you already have MacBest installed into your minetest (like 
.../someplace/.minetest/mods/macbest), you can now "Open Or Import" 
that folder so you can work on it using the IDE.  

If not, then you can select the IDE to get MacBest "From Version 
Control" using it's URL: https://bitbucket.org/steamcentral/macbest.git

Either way, you are likely to run into a problem here because your 
minetest mods folders might now show up when your IDE asks you 
for it.  If so, it helps to create a shortcut from your home 
or Desktop folder to your Minetest mods folder.

In some cases, depending on what installation method you used 
for Minetest and IntelliJ IDEA, you might run into security issues
with different applications not being allowed to see each other's
files.  Usually that is a good thing, but if you are using FlatPack
distributions of both Minetest AND IDEA, then you are out of luck.


## Telling your IDE What Python and LUA to Use

After you have opened your local copy of the macbest mod as an 
IntelliJ IDEA project, select File / Project Structure / Project Settings / 
SDK and make this project a Python3 venv project using the drop-down on 
the right hand side.

Next, select "Facets" and add a LUA 5.1 facet to your project.

At some point, the IDE is going to ask you to configure your Python and 
LUA setup for this project, which is just a matter of going along with
what it is suggesting.


## Telling your IDE about the Minetest API

Finally, you can tell your IDE about the minetest Lua source code
that comes with macbest, which will make LOA coding easier later.

Once you are done with those, look for a gear icon on the bottom 
of the window, then select New Project Structure.

From the top menu, select File->Project Structure->Global Libraries, then
click on '+' to add the LUA Zip Library located in dev/minetest5_buildin.zip


## Make sure your LUA works

In the IDE, you should see a list of macbest files on the left side bar.  
If not, click on "1:Project" (top left of window) to expose the side bar.

You should now be able to open _init.lua_ by double clicking on the 
file name.

You should see the LUA text highlighted in colour.  Scroll to the bottom of the
file, add a line and start typing "minetest.".

Once you get to the '.', the IDE will show you all the minetest functions
your code can call.  If you see a long list of function names like _check_player_privs()_,
then you know your code editor knows about LUA and about all the built-in minetest 
functions your mod can use to examine or change the voxel world.


## Make sure your Python works

If we did the Python part of the IDE install correctly, we should now be 
able to build a sky boxe from a digital image.

Open _tools/skydicer.py_ by double clicking on the file name in the side bar.

You might have to expand the _tools_ directory to see the files within.

From the top menu, select "Run->Run->1:skydicer->Run".

You will probably get an error, like 

```
Traceback (most recent call last):
  File ".../minetest/mods/macbest/tools/skydicer.py", line 3, in <module>
    import click
ModuleNotFoundError: No module named 'click'

Process finished with exit code 1
```

This is because your Python environment is missing some libraries.  You can fix
that by hovering your mouse over the troublesome lines in the python program 
skydicer.py and then choose "install package ...".

You will need to do the same for _numpy_, _Pillow_ (not PIL), and _py360convert_.

For some of these you may get errors.  If so, google "python3 install pillow windows" 
or whatever is likely to help you find the right clue.

When you try to run it again -- you can now use the green triangle on the top menu bar -- you
get a different error:

```
Usage: skydicer.py [OPTIONS] PANO_PATH DICE_DIR
Try 'skydicer.py --help' for help.

Error: Missing argument 'PANO_PATH'.
```

Apparently you did not give it the location of a file to convert.  We can fix this
by tweaking what it means to "Run" this code: select Run->Edit Configurations->1:skydicer
to make the following changes:

* set the parameter list to "skies/dustTempLight.png textures" (use \ instead of / on windows)
* set the working directory to ".../macbest" instead of .../macbest/tools"

and try again.  Now you should see:

```
writing  textures/dustTempLightFront.jpg
writing  textures/dustTempLightRight.jpg
writing  textures/dustTempLightBack.jpg
writing  textures/dustTempLightLeft.jpg
writing  textures/dustTempLightUp.jpg
writing  textures/dustTempLightDown.jpg

Process finished with exit code 0
```

Congratulations.  You just learned how to debug and run home-made Python scripts from the development environment.



## Check out that Sky Box!

The macbest mod needs customized (fixed and enhanced) versions both the 
skybox and geo-mapgen Minetest mods.  _Use File->New->Project From Version 
Control_ to install them into your Minetest mod directory next to macbest.

* https://bitbucket.org/steamcentral/skybox.git -> .../someplace/mods/skybox
* https://bitbucket.org/steamcentral/geo-mapgen.git -> .../someplace/mods/geo-mapgen

Fire up your Minetest and create a Minetest game world, but before you start it, "Configure" it
to enable the following mods:

* macbest
* skybox

Now, fire up your creative no-damage world and give yourself all privileges 
(including the skybox privilege) using the _grant_ chat command:

```
   /grant singleplayer all
```

If all went according to plan -- it rarely does the first time :-) -- you should now be 
able to load the sky box:

```
   /skybox dustTempLight
```

If you get an error and some hideously colored sky, then something went wrong with the Python 
script.  On the other hand, if all went well, you should see a sky showing the temperature of 
interstellar dust particles as seen from the earth, with the Minetest horizon corresponding to 
our galactic plane.

Feel free to doodle your own sky box:

* crayon doolde or water color onto a piece of paper
* scan in a rectangle as a png or jpg image and put it on your hard drive as crazyDoodle.png or crazyDoodle.jpg 
* add a new run definition for the tools/skydicer.py script to point that picture instead of the one in skies/
* run it
* add an entry to the list defined in skybox.lua for your new sky box texture, something like

```
    [...]
    macbest.skies = {
      ["withBird"] = {},
      ["crazyDoodle"] = {},  # <- add this
      ["earthatnight2012light"] = { "#882200", 0.8, { 
              density = 0.1
    [...]
```
* restart your world and say...

```
   /skybox
```

and the list should now include your new crazyDoodle sky box!


# Help?!

The web is your friend.  You will quickly learn how find answers 
to strange error messages by using the right search strings.

Also, if you get stuck, text/email/call me and I will do what I 
can to help.



