-- MacBest mod by helmut@zeebar.com
-- Based on node_ownership
-- License: LGPLv2+

macbest = {}

macbest.adminPrivs = {macbest=true}

macbest.startTime = os.clock()

macbest.modpath = minetest.get_modpath("macbest")

dofile(macbest.modpath.."/settings.lua")
dofile(macbest.modpath.."/chatcommands.lua")
dofile(macbest.modpath.."/skybox.lua")

macbest.set_skybox = function(player, sbn)
  skybox.set(player, sbn)
end

minetest.register_on_joinplayer(function(player)
  macbest.set_skybox(player, 0)
end)

minetest.register_privilege("macbest", {
	description = "Can administer macbest."
})


