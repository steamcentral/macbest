# MacTest module for MineTest 5

This Minetest was written to help some middle school students act out 
The Scottish Play in a mid evil castle of their own making.  

Some assembly is required.  For instance, you will need to collect
or create landscape data and the castle itself.  Some Python and
LUA coding may be involved.


## What you will need

on your computer: ...

* git 
* a working Minetest where you can create your own worlds
* Python3 and LUA for coding
* The IntelliJ IDE with Python and LUA plug-ins
* a web browser (like Google Chrome)
* some extra LUA and Python3 code installed
* some open source data and images from the web

yourself: ...

* some time and patience
* sense of curiosity
* sense of humour
* willingness to learn something new 


## Installation

This mod should be installed into your personal Minetest mod/
folder.  The location of that folder depends on how and where
your Minetest is installed.

* Max OS/X: $HOME/Library/Application Support/minetest/mods
* Linux: $HOME/.var/net.minetest/.minetest/mods
* Windows: ???

```
    cd $HOME/Library/Application\ Support/minetest/mods
    git clone https://bitbucket.org/steamcentral/macbest.git
```

You will also need to install the "skybox" and "geo-mapgen" mods.

```
    git clone https://bitbucket.org/steamcentral/skybox.git
    git clone https://bitbucket.org/steamcentral/geo-mapgen.git
```

You can install these using git (typing into a command window) or 
using an IDE.  

See install.md for more information on where to get things and where
to install them.  There are quite a few, but they are free and they
can be very useful for many other projects.

See https://dev.minetest.net/Installing_Mods for general information
on how to install Minetest Mods.  

Maybe a good way to start is to download Minetest (from minetest.net)
and to google Minetest mods.


## World Creation

Once you have the necessary pieces istalled.

* create a new world using the Minetest game (do not load yet)
* configure the world to use the skybox and macbest mods.  If
  the macbest mod does not show up on the list of available mods,
  something went wrong during installation
* now load (play) the game you have created and give yourself privileges using the "/grant singleplayer all" chat command
* you should now be able to see all available skies using the "/skybox" command


## Configuration

TODO

If you wish to specify configuration options, you should check 
settings.lua and set the appropriate settings in your server's 
configuration file (probably 'minetest.conf').


## Tools

Contains python tools for managing above data.

* image conversion
* consistency checking




## Skies

sky box image data (dice) are generated from panoramic images 
in skies/ using tools/skydice.py.

```
    tools/skydicer.py
```


## Sounds

none yet


## Landscape

Under the ground directory, you will find files describing the 
different locations in which the story unfolds.  These locations
may be used more than once and with different decorations, times
of day, weather, etc.

There is a subdirectory for each sub-area. For instance, things
only dealing with dunsinane hill, would be under ground/d_hill.

Here you will find ....

- GeoTIF files (.tif) which contain raster images where the value
  of each pixel represents the elevation
- landcover raster images (.gif ot .tif) where the color value
  represents the typo of biome to use for each peace of ground


## Characters

Under the chars/ directory, we have data and code for managing 
characters (players or non-player entities) for the play.


## Story

This directory contains character dialog and any other information 
necessary to capture how the story is to be acted out.

## Commands



## Miney Bots

Hopefully coming soon...

To connect to the game from a script (nice for building things), as
can use miney as long as that is also installed on the game server.

You need miney 2.0 installed because the publicly available python
module is 1.3 which will not work.

```
    python3 -m pip install --user git+https://github.com/miney-py/miney.git
```

Then, you can do this in Python:

```
>>> import miney
>>> mt = miney.Minetest("138.68.23.137", "admin", "me")
>>> pos = mt.player[0].position
{'y': 299.5, 'x': 1348.5230712891, 'z': -3266.5749511719}
>>> dry_grass = mt.node.type.default.dry_grass_5
>>> mt.node.get(pos)
{'param1': 15, 'name': 'air', 'param2': 0}
>>> mt.node.set(pos, None, dry_grass)
```

There are still some problems (bugs) with the required matching 
mineysocket mod which will make your Mintest server explode, 
but hopefully this will be useable soon.


## License

Copyright (C) 2020 Helmut Hissen helmut@zeebar.com.  All Rights Reserved


