local world_path = minetest.get_worldpath()

macbest.config = {}

local function setting(tp, name, default)
	local full_name = "macbest."..name
	local value
	if tp == "boolean" then
		value = minetest.settings:get_bool(full_name)
	elseif tp == "string" then
		value = minetest.settings:get(full_name)
	elseif tp == "position" then
		value = minetest.setting_get_pos(full_name)
	elseif tp == "number" then
		value = tonumber(minetest.settings:get(full_name))
	else
		error("Invalid setting type!")
	end
	if value == nil then
		value = default
	end
	macbest.config[name] = value
end

--------------
-- Settings --
--------------

setting("string", "filename", world_path.."/macbest.dat")

