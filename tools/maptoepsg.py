#!/usr/bin/env python3

import numpy as np
import rasterio
from rasterio.warp import calculate_default_transform, reproject, Resampling

import click

dst_crs = 'EPSG:4326'

def convert_to_epsg( inpath, outpath ):

  with rasterio.open(inpath) as src:

    transform, width, height = calculate_default_transform(src.crs, dst_crs, src.width, src.height, *src.bounds)
    kwargs = src.meta.copy()
    kwargs.update({
      'crs': dst_crs,
      'transform': transform,
      'width': width,
      'height': height
    })

    print("size: H", height, " W ", width)

    with rasterio.open(outpath, 'w', **kwargs) as dst:
      for i in range(1, src.count + 1):
        reproject(
          source=rasterio.band(src, i),
          destination=rasterio.band(dst, i),
          src_transform=src.transform,
          src_crs=src.crs,
          dst_transform=transform,
          dst_crs=dst_crs,
          resampling=Resampling.nearest)

@click.command()
@click.argument("inpath", type=click.Path(exists=True))
@click.argument("outpath", type=click.Path())
def maptoepsg(inpath, outpath):
  convert_to_epsg(inpath, outpath)

if __name__ == "__main__":
  maptoepsg()
