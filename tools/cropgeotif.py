#!/usr/bin/env python3

import rasterio
import click

@click.command()
@click.argument("inpath", type=click.Path(exists=True))
@click.argument("outpath", type=click.Path())
@click.option("--north", help="northern boundary", default=None, type=click.FLOAT)
@click.option("--east",  help="eastern boundary",  default=None, type=click.FLOAT)
@click.option("--south", help="southern boundary", default=None, type=click.FLOAT)
@click.option("--west",  help="western boundary",  default=None, type=click.FLOAT)
def cropgeo(inpath, outpath, north, east, south, west):

    with rasterio.open(inpath) as src:
        window = src.window( west, south, east, north )
        kwargs = src.meta.copy()
        kwargs.update({
            'height': window.height,
            'width': window.width,
            'transform': rasterio.windows.transform(window, src.transform)})
        with rasterio.open(outpath, 'w', **kwargs) as dst:
            dst.write(src.read(window=window))


if __name__ == "__main__":
    cropgeo()


