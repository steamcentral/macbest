#!/usr/bin/env python3

import click
import numpy as np
import PIL
from PIL import Image, ImageEnhance
import os


@click.command()
@click.argument("skin_path", type=click.Path(exists=True))
def ghostify(skin_path):
    skin_name = os.path.splitext(os.path.basename(skin_path))[0]
    skin_img = Image.open(skin_path).convert(mode='RGBA')
    gray_skin_img = ImageEnhance.Color(skin_img).enhance(0.0)
    gray_skin = np.array(gray_skin_img)
    gray_skin[:,:,3] = np.clip( gray_skin[:,:,3] - gray_skin[:,:,0] // 2 + 60, 0, 255 )
    macbest_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    texture_dir = os.path.join(macbest_root, "textures")
    ghost_texture_path = os.path.join(texture_dir, f"ghost_of_{skin_name}.png")
    Image.fromarray(gray_skin).save(ghost_texture_path)



if __name__ == "__main__":
    ghostify()

#
# skies_dir = os.path.join(macbest_root, "skies")
# for skies_file in os.listdir(skies_dir):
#  if os.path.splitext(skies_file)[1].lower() in [".jpg", ".png"]:
#    img_path = os.path.join(skies_dir, skies_file)
#    print("building skybox for ", skies_file)
#    buildSkyBox(img_path, texture_dir)
