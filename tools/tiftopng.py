#/usr/bin/env python3

import rasterio

with rasterio.open("d_hill_district.tif", "r") as infile:
  profile=infile.profile
  print( profile )
  profile['driver']='PNG'
  raster=infile.read()
  with rasterio.open("d_hill_district.png", 'w', **profile) as dst:
    dst.write(raster)


