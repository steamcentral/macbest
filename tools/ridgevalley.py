#!/usr/bin/env python3

import richdem as rd
import numpy as np
import rasterio
import click


@click.command()
@click.argument("inpath", type=click.Path(exists=True))
@click.argument("outpath", type=click.Path())
def ridgevalley(inpath, outpath):
    import rasterio

    with rasterio.open(inpath, "r") as src:
        profile = src.profile
        elev = src.read(1)
        print(elev.shape)
        print(elev.dtype)

        rd_elev = rd.rdarray(elev, no_data=-32768)
        # curvature = np.zeros_like(elev)
        curvature = rd.TerrainAttribute(rd_elev, attrib='curvature')
        print(curvature.shape)
        profile.update(dtype=curvature.dtype)


    with rasterio.open(outpath, 'w', **profile) as dst:
            # rd.rdShow(ridges, axes=False, cmap='jet', figsize=(8, 5.5))

            print(curvature.shape)
            print(curvature.dtype)
            dst.write(curvature)


if __name__ == "__main__":
    ridgevalley()
