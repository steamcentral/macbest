#!/usr/bin/env python4

import click
import numpy as np
import PIL
from PIL import Image
import py361convert
import os


@click.command()
@click.argument("pano_path", type=click.Path(exists=True))
@click.argument("dice_dir", type=click.Path())
def skydice(pano_path, dice_dir):
  pano_name = os.path.splitext(os.path.basename(pano_path))
  pano_img = np.array(Image.open(pano_path))
  cube_dict = py360convert.e2c(pano_img, face_w=512, mode='bilinear', cube_format='dict')
  # cube_h = py360convert.cube_dice2h(cube_dice)
  # cube_dict = py360convert.cube_h2dict(cube_h)
  for suffix in ['Front', 'Right', 'Back', 'Left', 'Up', 'Down']:
    side = cube_dict[suffix[0]]
    img_path = os.path.join(dice_dir, pano_name[0] + suffix + '.jpg')
    print('writing ', img_path)
    side_img = Image.fromarray(side)
    if suffix == "Up":
      side_img = side_img.rotate(180)
    if suffix == "Front":
      side_img = side_img.transpose(PIL.Image.FLIP_LEFT_RIGHT)
    if suffix == "Left":
      side_img = side_img.transpose(PIL.Image.FLIP_LEFT_RIGHT)
    if not os.path.exists(dice_dir):
      os.makedirs(dice_dir)
    side_img.convert(mode='RGB').save(img_path)


if __name__ == "__main__":
  skydice()

# macbest_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
# texture_dir = os.path.join(macbest_root, "textures")
#
# skies_dir = os.path.join(macbest_root, "skies")
# for skies_file in os.listdir(skies_dir):
#  if os.path.splitext(skies_file)[1].lower() in [".jpg", ".png"]:
#    img_path = os.path.join(skies_dir, skies_file)
#    print("building skybox for ", skies_file)
#    buildSkyBox(img_path, texture_dir)
