# Skies

Sky boxes can be generated from panorama images which can be 
rengdered using ray tracers, stiched together from digital photos,
or found on the web. It helps to have a few sample images to 
get started.

## Examople Data Included Here

Sample data for sky boxes was obtained from the following sources:

### withBird.png

This image is courtesy of Patrick who drew this with pencil crayon 
for me on very short notice in order to demonstrate how sky boxes 
work to a group of students studyig The Scottish Play.

### dustTempLight.png

The original image was downlaoded as a false color (rainbow) image 
from [CalTech|http://planck.ipac.caltech.edu/image/planck10-001b], 
with credit to ESA and the HFI Consortium.  It represents the
temperature of dust with respect to the galactive plane.

I was motified to decrease color saturation.

### earthatnight2012light.png

The Earth at Night series of global mosaic images covering are availbleA
from [NASA|https://earthobservatory.nasa.gov/features/NightLights]

# (khM4e.png)

360 degree equirectangular projection of all naked eye stars with precessional motion for simulating


[https://astronomy.stackexchange.com/questions/21574/360-degree-equirectangular-projection-of-all-naked-eye-stars-with-precessional-m]


## Other Data Sources

Many other interesting equirectangular maps can be found online. While
the dimensions or aspect ratios do not matter, they do need to be
equirectangular or convertable into a a equirectanugular
projectioprojection.

- [https://commons.wikimedia.org/wiki/File:Constellations_ecliptic_equirectangular_plot.svg]
- [https://www.jpl.nasa.gov/spaceimages/details.php?id=PIA15482]
- [https://en.wikipedia.org/wiki/Constellation#/media/File:Hipparcos_Catalogue_equirectangular_plot.svg]
- [http://planck.ipac.caltech.edu/image/planck13-002c]
- [https://www.ips-planetarium.org/page/fulldomestills]
- [https://www.usgs.gov/science-explorer-results?es=Hemispherical+Photo]
- [https://informal.jpl.nasa.gov/museum/360-video]

Some of these require processing with image editing / drawing tools
to be clipper, turned into raster images, to reproject, or simply 
to make look good as a sky.


