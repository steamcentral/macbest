
import math
import numpy as np
import rasterio
# import richdem as rd
import matplotlib.pyplot as plt
from os import path

from typing import NamedTuple
# from skimage import color, morphology
from skimage.io import imread

my_dir = path.dirname(path.abspath(__file__))
clc_codes_path = path.join(path.dirname(path.dirname(path.dirname(my_dir))), "geo-mapgen", "Land cover tables", "clc.lct")


#
#  Biomes for Corine Land Cover maps. https://land.copernicus.eu/pan-european/corine-land-cover:
#
#   industrial urban
#   rock gravel sand dirt
#   grass dry_grass fields scrub
#   bushes mixed_forest deciduous_forest coniferous_forest
#   wetland water ice sea
#

def load_land_cover_codes( lcc_path ):
  lc_dict = {}
  with open(lcc_path, 'r') as lcc_reader:
    for line in lcc_reader.readlines():
      words = [w for w in line.split() if w != ""]
      if len(words) == 2:
        lc_dict[words[1]] = int(words[0])
  return lc_dict


def create_circular_mask(h, w, center=None, radius=None):
  if center is None: # use the middle of the image
    center = (int(w/2), int(h/2))
  if radius is None: # use the smallest distance between the center and image walls
    radius = min(center[0], center[1], w-center[1], h-center[0])
  Y, X = np.ogrid[:h, :w]
  dist_from_center = np.sqrt((X - center[1] + 0.5)**2 + (Y-center[0] + 0.5)**2)
  mask = dist_from_center <= radius
  return mask


class Biome(NamedTuple):
  name: str
  color: str
  land_cover_name: str


with rasterio.open('island_dsm.tif') as dsm_geotif:
  dsm = dsm_geotif.read(1)

with rasterio.open('island_dtm.tif') as dtm_geotif:
  dtm = dtm_geotif.read(1)
  dtm_profile = dtm_geotif.profile

lab_img = imread('labyrinth_80.jpg', as_gray=True)

land_cover_codes = load_land_cover_codes( clc_codes_path )

biomes = { bd[0] : Biome( bd[0], bd[1], bd[2] ) for bd in [
  [ 'ocean', (  80,  80, 220 ), 'sea'],
  [ 'beach', ( 200, 180, 120 ), 'sand'],
  [ 'hedge', (  80, 180,  80 ), 'scrub'],
  [ 'path',  ( 120,  80,  60 ), 'dirt'],
  [ 'grass', (  40, 180,  20 ), 'grass'],
  [ 'rock',  ( 180, 220,  60 ), 'fields'],
  [ 'trees', (  20, 120,  60 ), 'coniferous_forest'],
  [ 'pond',  ( 180, 180, 250 ), 'wetland']
]}

r_edge = 8

ground_height, ground_width = dtm.shape

lab_center = [l // 2 for l in dtm.shape]

lab_ri = max([k // 2 for k in lab_img.shape])
lab_ro = lab_ri + r_edge

lab_offset = [lab_center[d] - lab_img.shape[d]//2 for d in range(2)]

spawn_pad_r = max(lab_ro / 4, r_edge * 3 // 2)
spawn_pad_o = lab_ro + 2 * spawn_pad_r // 3
spawn_pad_a = math.radians(0)
spawn_pad_center = [lab_center[d] + spawn_pad_o * math.sin(spawn_pad_a + d * math.radians(90)) for d in range(2)]

lab_elev = 2
lab_hedge_height = 2

spawn_pad_mask = create_circular_mask( dtm.shape[0], dtm.shape[1], spawn_pad_center, spawn_pad_r )

lab_hedge_mask = lab_img < 0.2
lab_green_mask = lab_img < 0.6

ocean_margin = 20

land_mask = dtm >= 0.1
land_mask[:ocean_margin,:] = False
land_mask[ground_height - ocean_margin:,:] = False
land_mask[:,:ocean_margin] = False
land_mask[:,ground_width-ocean_margin:] = False

tclip = max(dtm.shape) // 2

for n in range(1,tclip):
  land_mask[ground_height - n, ground_width - tclip + n:] = False

ocean_mask = np.ones(dtm.shape, dtype=bool)
ocean_mask[land_mask] = False

ro_mask = create_circular_mask( dtm.shape[0], dtm.shape[1], lab_center, lab_ro )
not_ro_mask = np.ones(dtm.shape, dtype=bool)
not_ro_mask[ro_mask] = False

tree_mask = dtm > 4
tree_mask[ocean_mask] = False

pond_r = 3
pond_mask = create_circular_mask( dtm.shape[0], dtm.shape[1], lab_center, pond_r )

lab_ys = range(lab_img.shape[0])
ground_lab_xs = range(lab_offset[1], lab_img.shape[1] + lab_offset[1])

path_mask = np.zeros(dtm.shape, dtype=bool)

path_mask[ro_mask] = True
path_mask[spawn_pad_mask] = True

beach_mask = dtm < 0.6
beach_mask[ocean_mask] = False
beach_mask[path_mask] = False


hedge_mask = np.zeros(dtm.shape, dtype=bool)
green_mask = np.zeros(dtm.shape, dtype=bool)

for y in lab_ys:
  hedge_mask[y + lab_offset[0], ground_lab_xs] = lab_hedge_mask[y,:]
  green_mask[y + lab_offset[0], ground_lab_xs] = lab_green_mask[y,:]

green_mask[not_ro_mask] = False

biome_color_map = np.zeros([dtm.shape[0], dtm.shape[1], 3], dtype=np.uint8)
biome_code_map = np.zeros([dtm.shape[0], dtm.shape[1]], dtype=np.uint8)

color_map = {
  'ocean': ocean_mask,
  'rock': land_mask,
  'path': path_mask,
  'grass': green_mask,
  'hedge': hedge_mask,
  'trees': tree_mask,
  'pond' : pond_mask,
  'beach' : beach_mask
}

biome_code_map[:,:] = land_cover_codes["sea"]

for biome_name, mask in color_map.items():
  biome = biomes[biome_name]
  biome_color_map[mask] = biome.color
  land_cover_code = land_cover_codes[biome.land_cover_name]
  if land_cover_code != None:
    biome_code_map[mask] = land_cover_code

dtm[ocean_mask] = 0.0
dtm[path_mask]  = lab_elev
dtm[hedge_mask] = lab_elev + lab_hedge_height

fig, ax = plt.subplots(ncols=2, figsize=(20, 8))

ax[0].set_title('Elevation')
ax[0].imshow(dtm, cmap='gray')

ax[1].set_title('Biomes')
ax[1].imshow(biome_color_map)

with rasterio.open('island_elev.tif', 'w', **dtm_profile) as elev_geotif:
  elev_geotif.write(dtm.astype(rasterio.float32), 1)

land_cover_profile = dtm_profile.copy()
land_cover_profile.update(dtype=rasterio.uint8, compress='lzw', nodata=255)

with rasterio.open('island_cover.tif', 'w', **land_cover_profile) as cover_geotif:
  cover_geotif.write(biome_code_map.astype(rasterio.uint8), 1)

print( 'spawn_point [X,Z] is ', spawn_pad_center[0], spawn_pad_center[1] - ground_height )

plt.show()

