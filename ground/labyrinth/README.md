
# Labytrinth Example

We are building a labyrinth on a tiny island off Vancouver Island, east of Nanaimo.

* clip --west 430250 --south 5453240 --east 430740 --north 5453690
* black out 20 rows and columns along the edge
* run build_map.py to build the height and biome GEOTiffs, note spawn point 
* create a new empty world in minetest with geo-mapgen, skybox, and macbest mods enabled
* generate the height/biome data file used in the geo-mapgen lazy mapgen
```
     cd ~/.minetest/mods/geo-mapgen
     ./map_convert.py ../macbest/ground/labyrinth/island_elev.tif \
           --landcovermap ../macbest/ground/labyrinth/island_cover.tif \
           --landcoverlegend Land\ cover\ tables/clc.lct --no-genrivers \
           --scale 1 --spawnx 225 --spawny 4 --spawnz -149 ../../worlds/island01 
```
* set world spawn point 
* play game

# References

DEM from the Canadian High Resolution Elevation Open Data Repo

* 1m/VILLE_NANAIMO/VILLE_NANAIMO/utm10/dtm_1m_utm10_w_6_145.tif
* bounds are [430000.0, 5450000.0, 440000.0, 5460000.0]
* island upper left corner is at 510x6420, size is 200x200
* island bbox is west 430530 south 5453420 east 430650 north 5453540

```
    wget https://ftp.maps.canada.ca/pub/elevation/dem_mne/highresolution_hauteresolution/dtm_mnt/1m/VILLE_NANAIMO/VILLE_NANAIMO/utm10/dtm_1m_utm10_w_6_145.tif
    ../../tools/cropgeotif.py dtm_1m_utm10_w_6_145.tif island_dtm.tif --west 430530 --south 5453420 --east 430650 --north 5453540
    wget https://ftp.maps.canada.ca/pub/elevation/dem_mne/highresolution_hauteresolution/dsm_mns/1m/VILLE_NANAIMO/VILLE_NANAIMO/utm10/dsm_1m_utm10_w_6_145.tif
    ../../tools/cropgeotif.py dsm_1m_utm10_w_6_145.tif island_dsm.tif --west 430530 --south 5453420 --east 430650 --north 5453540
```


