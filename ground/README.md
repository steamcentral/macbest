# Ground

These are the physical locations for which we would like to generate 
landscapes

## Workflow

Both the top detail and the landcover can be hand-painted using 
a vector based drawing tool like Inkscape.

The first step is to get a reference areal photo and/or map which
serves as a painting background picture.  You can then draw over 
top of the picture using vector graphics (lines, polygons) and fill
these objest with certain colours.

The reference photo will be called SOMENANE_ref.png

## Links

Here are some interesting links

- [https://tangrams.github.io/heightmapper]


