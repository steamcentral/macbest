
# Dunsinane Hill

The region is 56.46 .. 56.49 North , -3.30 .. -3.26 West, covering 
approx 8 square kilometres.


```
    cropgeotif.py n56_w004_1arc_v3.tif d_hill_elev.tif --south 56.46 --north 56.49 --west -3.30 --east -3.26
    cropgeotif.py n56_w004_1arc_v3.tif d_hill_elev.tif --south 56.46 --north 56.49 --west -3.30 --east -3.26
```

# Air Photo

We will be using an Air photo off the web and prune it a bit.

- [bing|https://www.bing.com/maps/?mkt=en-gb&v=2&cp=56.46993~-3.279017&lvl=14&sty=o]

This gives us the d_hill_ref.png

# Tangrams

The Tangrams heightmapper can help us line up the ref photo with
rivers and elevations as well (see heightmapper-1591205645315.png)
and we will use that in the svg as well.




