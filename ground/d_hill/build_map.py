# img_interp.py
import os
import numpy as np
from os import path
from scipy.ndimage import gaussian_filter, median_filter
import matplotlib.pyplot as plt
from PIL import Image
import rasterio
from rasterio.transform import Affine

# Read in image and convert to greyscale array object
epro_name = "d_hill_epro.png"
epro_png = Image.open(epro_name)
epro = np.array(epro_png)

green_name = "d_hill_green.png"
green_png = Image.open(green_name)
green = np.array(green_png)[:,:,1]

epro_west = -3.2780493
epro_east = -3.2761823

epro_south = 56.4700224
epro_north = 56.4712178

epro_half = (epro_north + epro_south) / 2
epro_yscale = 1.1

print( epro_south, epro_north )

epro_south = epro_half + (epro_south - epro_half) * epro_yscale
epro_north = epro_half + (epro_north - epro_half) * epro_yscale

print( epro_south, epro_north )


epro_gray  = epro[:,:,0]
epro_alpha = epro[:,:,3]

epro_mask = epro_alpha >= 255
epro_bad = epro_alpha < 255

epro_gray[epro_bad] = 0

epwa = np.zeros_like(epro_gray)



from typing import NamedTuple


def load_land_cover_codes( lcc_path ):
  lc_dict = {}
  with open(lcc_path, 'r') as lcc_reader:
    for line in lcc_reader.readlines():
      words = [w for w in line.split() if w != ""]
      if len(words) == 2:
        lc_dict[words[1]] = int(words[0])
  return lc_dict


class Biome(NamedTuple):
  name: str
  color: str
  land_cover_name: str

my_dir = path.dirname(path.abspath(__file__))
clc_codes_path = path.join(path.dirname(path.dirname(path.dirname(my_dir))), "geo-mapgen", "Land cover tables", "clc.lct")

land_cover_codes = load_land_cover_codes( clc_codes_path )


(ny, nx) = epwa.shape

x = np.linspace(epro_west, epro_east, nx)
y = np.linspace(epro_south, epro_north, ny)

X, Y = np.meshgrid(x, y)
x_res = (x[-1] - x[0]) / nx
y_res = (y[-1] - y[0]) / ny

transform = Affine.translation(x[0] - x_res / 2, y[0] - y_res / 2) * Affine.scale(x_res, y_res)

print(green[50])

# Create a figure of nrows x ncols subplots, and orient it appropriately
# for the aspect ratio of the image.
nrows, ncols = 2, 2
fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(6,4), dpi=100)
if nx < ny:
  w, h = fig.get_figwidth(), fig.get_figheight()
  fig.set_figwidth(h), fig.set_figheight(w)

# Convert an integer i to coordinates in the ax array
get_indices = lambda i: (i // nrows, i % ncols)

# Sample 100, 1,000, 10,000 and 100,000 points and plot the interpolated
# images in the figure

npasses = [0, 1, 10, 100]

ps = 0

print( epro_gray.shape )
print( epro_mask.shape )
print( epro_alpha.shape )
print( epwa.shape )

epwa = epro_gray * 1
epwa[epro_bad] = 128

for i in range(4):
  axes = ax[get_indices(i)]
  while ps < npasses[i]:
    epwa = gaussian_filter(median_filter(epwa, 4), 3.0)
    epwa[epro_mask] = 0
    epwa = epwa + epro_gray
    ps = ps + 1
  epwa = gaussian_filter(epwa, 2.0)
  axes.imshow(Image.fromarray(epwa))
  axes.set_xticks([])
  axes.set_yticks([])
  axes.set_title('passes = {0:d}'.format(ps))

filestem = os.path.splitext(os.path.basename(epro_name))[0]
plt.savefig('{0:s}_interp.png'.format(filestem), dpi=100)

#
# input (green):
#
# 99   rock
# 135  brush
# 157  grass
#
# output (grwa):
#
# grass
# rock
# fields
# coniferous_forest
#

grwa = np.full_like(green, land_cover_codes["fields"])

rock_mask = green > 142
grass_mask = green < 120

grwa[grass_mask] = land_cover_codes["grass"]
grwa[rock_mask] = land_cover_codes["rock"]

yscale = 2

# nx = 3
# ny = 3

x1 = nx - 2
x0 = 4

y1 = ny - 2
y0 = 2

maxy = np.max(epwa[y0:y1,x0:x1]) // yscale
miny = np.min(epwa[y0:y1,y0:y1]) // yscale

spawn_x = x1 - 1
spawn_y = y1 - 1

spawn_my = epwa[spawn_y, spawn_x] // yscale + 1
spawn_mx = spawn_x - x0
spawn_mz = spawn_y - y0

with open('blocks.csv', 'w') as blks:
  blks.write(f'0,0,0,min,\n')
  blks.write(f'{x1 - x0},{maxy - miny},{y1 - y0},max,\n')
  blks.write(f'{spawn_mx},{spawn_mz},{spawn_my},player,\n')
  for x in range(1, nx - 1):
    for y in range(1, ny - 1):
      mz = y - 1
      mx = x - 1
      my = epwa[y,x] // yscale - miny
      vnt = None
      gnt = "dirt_with_dry_grass"
      if grass_mask[y,x]:
        vnt = None
        gnt = "dirt_with_grass"
      if rock_mask[y,x]:
        vnt = None
        gnt = "sandstone"
      if not( vnt is None ):
        blks.write(f'{mx},{my + 1},{mz},{vnt},\n')
      yl = 20
      while my >= 0 and yl > 0:
        blks.write(f'{mx},{my},{mz},{gnt},\n')
        gnt = "stone"
        my = my - 1
        yl = yl - 1



with rasterio.open( 'd_hill_epro.tif', 'w', driver='GTiff', height=epwa.shape[0], width=epwa.shape[1], count=1, dtype=epwa.dtype, crs='+proj=latlong', transform=transform ) as epro_tif:
  epro_tif.write(epwa, 1)
with rasterio.open( 'd_hill_green.tif', 'w', driver='GTiff', height=grwa.shape[0], width=grwa.shape[1], count=1, dtype=grwa.dtype, crs='+proj=latlong', transform=transform ) as green_tif:
  green_tif.write(grwa, 1)
